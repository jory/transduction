#include "params/params.h" /// command line param parsing
#include <iostream> /// terminal input output
#include <vector>
#include <list>
#include <string> /// filenames etc.
#include <cstdlib> /// rand,srand
#include <cassert> /// assert
#include <random> /// binomial_distribution, random
#include <algorithm> /// iter_swap, sort, unique
#include <typeinfo>
#include <iterator> /// advance
#include <ncurses.h> /// terminal display
#include <csignal> /// ctrl-c trap
#include <cctype> /// tolower of chars
#include <memory> /// shared_ptr

/// x-platform procid
#ifdef _WIN32
#include <process.h>
#else
#include <unistd.h>
#endif

/// Data Vault fields
#define FIELDS \
FIELD_STATIC_STRING( experiment ) \
FIELD_STATIC_INT( replicate ) \
FIELD_INT( update ) \
FIELD_INT( nHosts ) \
FIELD_INT( nPhages ) \
FIELD_INT( nSUHosts ) \
FIELD_INT( nSIHosts ) \
FIELD_INT( nRUHosts ) \
FIELD_INT( nRIHosts ) \
FIELD_INT( nTPhages ) \
FIELD_INT( nNPhages ) \
FIELD_INT( drugPresent ) \
FIELD_FLOAT( meanTransduction ) \
FIELD_FLOAT( maxTransduction) \
FIELD_STATIC_FLOAT( phageInfectionRate ) \
FIELD_STATIC_FLOAT( hostDeathRate ) \
FIELD_STATIC_FLOAT( phageDeathRate ) \
FIELD_STATIC_INT( hostRadius ) \
FIELD_STATIC_INT( phageRadius ) \
FIELD_STATIC_INT( burstSize ) \
FIELD_STATIC_INT( hostDoublingTime ) \
FIELD_STATIC_FLOAT( drugRate ) \
FIELD_STATIC_INT( intervalDelay ) \
FIELD_STATIC_INT( intervalOn ) \
FIELD_STATIC_INT( intervalOff ) \
FIELD_STATIC_INT( xDim ) \
FIELD_STATIC_INT( yDim ) \
FIELD_STATIC_FLOAT( resistanceCost ) \
FIELD_STATIC_FLOAT( initPhage1 ) \
FIELD_STATIC_FLOAT( initPhage2 ) \
FIELD_STATIC_INT( uniformMutations )
#include "vault/vault.h" /// data archiving and saving utility

namespace g {
	/// global simulation parameters
	float hostMu(0.001); /// host mutation rate
	float phageMu(0.01); /// phage mutation rate
	float hostDeathRate(0.001); /// rate of host death per update
	float phageDeathRate(0.006); /// rate of phage death per update
	float phageInfectionRate(0.0009); /// rate of phage infection per update
	int hostRadius(1); /// spatial effect (in grid cells)
	int phageRadius(1); /// spatial effect (in grid cells)
	int burstSize(50); /// phage burst size upone lyse
	int hostDoublingTime(100); /// doubling time for host cells (updates)
	int phageLyseDelay(100); /// after infection, before cell lysing
	float initHostsResistant(0.8); /// fraction of seed hosts that are resistant
	std::vector< float > initPhageType; /// fraction of seed phage and their transduction trait-value (float, float)
	std::vector< float > initPhageTypes; /// fraction of seed phage as type1 and type2, and their transduction trait-values (float, float, float, float)
	int updates(1000); /// num updates to simulate / end after
	std::string experiment("test"); /// experiment ID for recording purposes
	int replicate; /// replicate number for recording purposes
	float drugRate(0.9); /// rate drug affects S agents per update
	float resistanceCost(0.1); /// cost of being resistant to the drug
	std::vector< int > drugIntervals; /// [beginGeneration, generationsOn, generationsOff]
	std::vector< int > initHostPhage; /// initial numbers of host and phage
	std::string saveFilename("data.ssv"); /// filename to save data
	bool saveFileAppend(false); /// whether or not to append to the save file (respects headers)
	bool showHelp(false); /// --help
	bool visualize(false); /// enable curses visualization
	int xdim(32), ydim(32); /// dimensions of the 2D worlds
	bool debug(false);
	bool uniformMutations(false); /// how mutations of continuous traits are made, local about distribution by default, optionally uniform (only affects phage)
	/// global variables
	int update(0); /// current update
	bool drugPresent(false);
	int updatesSinceLastDrugEvent(0);
	std::default_random_engine generator;
	std::uniform_real_distribution<float> dUniform;
}

class World;
class Phage;

class Agent {
	friend class World;
	protected:
		World* world;
		int born; /// the update when born
	public:
		int x,y;
		bool hasResistanceAllele;
		Agent* ancestor;
		Agent();
		virtual void inheritFrom(std::shared_ptr<Agent> agent) = 0;
		virtual void reinit() = 0;
		std::ostream& operator<<(std::ostream& output);
};

class Host : public Agent {
	private:
	public:
		bool infected;
		std::shared_ptr<Phage> infector;
		int nextDoublingTime; /// next update when this agent will reproduce
		void setNextDoublingTime();
		static std::normal_distribution<float> dNormalDoubling;
		void inheritFrom(std::shared_ptr<Agent> agent) override;
		void reinit() override;

		Host();
};

class Phage : public Agent {
	public:
		float transductionRate;
		bool isTransducing;
		static std::normal_distribution<float> dNormalTransductionMu;
		static std::binomial_distribution<> dBinomialInfect;
		void inheritFrom(std::shared_ptr<Agent> agent) override;
		void reinit() override;

		Phage();
};

template <class T>
class Population : public std::list<std::shared_ptr<T>> {
	public:
		typedef typename std::list<std::shared_ptr<T>>::iterator iterator;
		//iterator begin() { return this->begin(); }
		//iterator end() { return this->end(); }
		std::binomial_distribution<>* dBinomialDeath;
		std::vector< std::vector< std::list<std::shared_ptr<T>> > > grid; /// for speed, keeps track of # of organisms in locations

		Population(unsigned int numInitElements, const T& value);
		Population(unsigned int numInitElements);
		Population();
		void printSize();

		void doMoranProcessDeath(float, bool ignoreResistant = false);
};

class World {
	friend class Agent;
	private:
		int xdim,ydim;
	public:
		Population<Host> hosts;
		Population<Phage> phages;
		World(int xdim, int ydim);
		template <class T>
		std::shared_ptr<T> tryAddAgent(Population<T> &population, bool sharesSpace=true, std::shared_ptr<Agent> parent=nullptr, int radius=1); /// sharesSpace=True if multiples allowed at one grid loc
		void seed(unsigned int numHosts, unsigned int numPhage);
		void step();
		void collectData();
};

std::ostream& operator<<(std::ostream& output, const Agent& agent) {
	output << "agent at position [" << agent.x << "," << agent.y << "]";
	return output;
}
std::ostream& operator<<(std::ostream& output, const Phage& agent) {
	output << "phage at position [" << agent.x << "," << agent.y << "]";
	return output;
}
std::ostream& operator<<(std::ostream& output, const Host& agent) {
	output << "host at position [" << agent.x << "," << agent.y << "]";
	return output;
}

std::normal_distribution<float> Host::dNormalDoubling;
std::normal_distribution<float> Phage::dNormalTransductionMu;
std::binomial_distribution<> Phage::dBinomialInfect;

volatile sig_atomic_t userExitFlag = 0;
void catchCtrlC(int signalID) {
	signal(SIGINT, catchCtrlC);
	userExitFlag = 1;
	if (g::visualize) {
		int width(0),height(0);
		getmaxyx(stdscr, height, width);
		std::string msg("(Press 'q' to quit)");
		mvaddstr(height-2, width-msg.length()-1, msg.c_str());
	}
}

enum class COLOR {suHost=1, ruHost, siHost, riHost, Phage, Black, White, Highlight, NoHighlight};
void initCursesMode() {
	initscr();
	if (not has_colors() ) std::cout << "WARNING: colors unsupported" << std::endl;
	else start_color();
	curs_set(0);
	noecho();
	timeout(0);
	/// see colors: http://stackoverflow.com/questions/18551558/how-to-use-terminal-color-palette-with-curses
	init_pair((short)COLOR::suHost, 27, 27); /// susceptible uninfected host (medium blue)
	init_pair((short)COLOR::ruHost, 15, 15); /// resistant uninfected host (white)
	init_pair((short)COLOR::siHost, 57, 57); /// susceptible infected host (purple/pink)
	init_pair((short)COLOR::riHost, 245, 245); /// resistant infected host (grey)
	init_pair((short)COLOR::Phage, 134, 134); /// phage with no host in immediate vicinity
	init_pair((short)COLOR::Black, 0, 0); /// black color
	init_pair((short)COLOR::White, 15, 15); /// resistant uninfected host (white)
	init_pair((short)COLOR::NoHighlight, 15, 0); // white foreground, black background
	init_pair((short)COLOR::Highlight, 0, 15); // black foreground, white background
}

void sadface() {
	/// shown when all populations crash
	static int sadness(0);
	++sadness;
	if (sadness > 4000) {
		sadness = 4001;
		int yo = g::ydim/2-5;
		int xo = g::xdim-5;
		mvaddstr(yo+0,xo,"    .-\"\"\"\"\"\"-.");
		mvaddstr(yo+1,xo,"  .'          '.");
		mvaddstr(yo+2,xo," /    ><  ><    \\");
		mvaddstr(yo+3,xo,":           `    :");
		mvaddstr(yo+4,xo,"|                |");
		mvaddstr(yo+5,xo,":     .-...-.,   :");
		mvaddstr(yo+6,xo," \\   `          /");
		mvaddstr(yo+7,xo,"  '.          .'");
		mvaddstr(yo+8,xo,"    '-......-'");
	}
}

void saveData() {
	VAULT::Record::experiment = g::experiment;
	VAULT::Record::replicate = g::replicate;
	VAULT::Record::drugRate = g::drugRate;
	VAULT::Record::xDim = g::xdim; VAULT::Record::yDim = g::ydim;
	VAULT::Record::intervalDelay = g::drugIntervals[0];
	VAULT::Record::intervalOn = g::drugIntervals[1];
	VAULT::Record::intervalOff = g::drugIntervals[2];
	VAULT::Record::burstSize = g::burstSize;
	VAULT::Record::phageInfectionRate = g::phageInfectionRate;
	VAULT::Record::phageDeathRate = g::phageDeathRate;
	VAULT::Record::hostDeathRate = g::hostDeathRate;
	VAULT::Record::hostDoublingTime = g::hostDoublingTime;
	VAULT::Record::hostRadius = g::hostRadius;
	VAULT::Record::phageRadius = g::phageRadius;
	VAULT::Record::resistanceCost = g::resistanceCost;
	if (g::initPhageTypes[0] < 0) {
		VAULT::Record::initPhage1 = g::initPhageType[0];
		VAULT::Record::initPhage2 = -1.0f;
	} else {
		VAULT::Record::initPhage1 = g::initPhageTypes[0];
		VAULT::Record::initPhage2 = g::initPhageTypes[1];
	}
	VAULT::save( g::saveFilename, !g::saveFileAppend );
}

int main(int argc, char* argv[]) {
	signal(SIGINT, catchCtrlC); /// set control-c exit trap

	{
		using namespace Params;
		addp(TYPE::BOOL, &g::showHelp);
		addp(TYPE::STRING, &g::experiment, g::experiment, false, "--experiment", "Experimental condition identifier for data recording.");
		addp(TYPE::INT, &g::replicate, "1", false, "--replicate", "Replicate ID for data recording.");
		addp(TYPE::FLOAT, &g::hostMu, std::to_string(g::hostMu), false, "--hostMu", "Host cell mutation rate (genomic).");
		addp(TYPE::FLOAT, &g::phageMu, std::to_string(g::phageMu), false, "--phageMu", "Phage mutation rate (genomic).");
		addp(TYPE::FLOAT, &g::drugRate, std::to_string(g::drugRate), false, "--drugRate", "Rate at which drug kills Susceptible cells each update.");
		addp(TYPE::FLOAT, &g::phageInfectionRate, std::to_string(g::phageInfectionRate), false, "--phageInfectionRate", "Probability for phage infection.");
		addp(TYPE::FLOAT, &g::resistanceCost, std::to_string(g::resistanceCost), false, "--resistanceCost", "Cost of having resistance, as \% of non-resistant type (0-1).");
		addp(TYPE::FLOAT, &g::initPhageType, 2, "-1", false, "--initPhageType", "Phage transduction value and its prevalence among the seed phage. Vals 0-1.");
		addp(TYPE::FLOAT, &g::initPhageTypes, 3, "-1", false, "--initPhageTypes", "Phage transduction values 1, 2, and prevalnce of 1. (prevalence of 2 is 1.0-p). Vals 0-1.");
		addp(TYPE::INT, &g::burstSize, std::to_string(g::burstSize), false, "--burstSize", "Phage burst size, number of phage particles released on lysing.");
		addp(TYPE::INT, &g::hostDoublingTime, std::to_string(g::hostDoublingTime), false, "--hostDoublingTime", "Host cell doubling time (updates).");
		addp(TYPE::INT, &g::hostRadius, std::to_string(g::hostRadius), false, "--hostRadius", "Host cell dispersal radius (in grid cells).");
		addp(TYPE::INT, &g::phageRadius, std::to_string(g::phageRadius), false, "--phageRadius", "Phage particle dispersal radius (in grid cells).");
		addp(TYPE::INT, &g::updates, std::to_string(g::updates), "--updates", "Number of simulation steps/updates to simulate.");
		addp(TYPE::INT, &g::xdim, std::to_string(g::xdim), "--xdim", "Width of 2D world/grid.");
		addp(TYPE::INT, &g::ydim, std::to_string(g::ydim), "--ydim", "Height of 2D world/grid.");
		addp(TYPE::INT, &g::drugIntervals, 3, "-1", false, "--drugIntervals", "DrugBeginGeneration GenerationsApplying GenerationsNotApplying Repeats applying and not applying after begin generation.");
		addp(TYPE::INT, &g::initHostPhage, 2, "10", false, "--initHostPhage", "Initial levels of host and phage individuals.");
		addp(TYPE::FLOAT, &g::hostDeathRate, std::to_string(g::hostDeathRate), false, "--hostDeathRate", "Rate of host death per update.");
		addp(TYPE::FLOAT, &g::phageDeathRate, std::to_string(g::phageDeathRate), false, "--phageDeathRate", "Rate of phage death per update.");
		addp(TYPE::FLOAT, &g::initHostsResistant, std::to_string(g::initHostsResistant), false, "--initHostsResistant", "Fraction of seed hosts that are drug resistant.");
		addp(TYPE::INT, &g::phageLyseDelay, std::to_string(g::phageLyseDelay), false, "--phageLyseDelay", "Delay of updates before cell lyses after infection.");
		addp(TYPE::BOOL, &g::debug, false, "--debug", "Debug mode (verbose output).");
		addp(TYPE::BOOL, &g::visualize, false, "--visualize", "Enable curses visulization mode.");
		addp(TYPE::STRING, &g::saveFilename, g::saveFilename, false, "--saveFilename", "Customimze the data filename.");
		addp(TYPE::BOOL, &g::saveFileAppend, false, "--saveFileAppend", "Toggles append mode to the save file. Respects headers.");
		addp(TYPE::BOOL, &g::uniformMutations, false, "--uniformMutations", "Forces uniform mutational landscape of transduction trait.");
	}

	/// Parse the command line options and do help info
	Params::argparse(argv);
	if (g::showHelp || (argc==1)) {
		std::cout << "Usage: " << argv[0] << " TODO example command here" << std::endl;
		std::cout << std::endl;
		std::cout << Params::argdetails();
		return(0);
	}
	if (g::initPhageType[0]+g::initPhageTypes[0] > std::fmaxf(g::initPhageType[0], g::initPhageTypes[0])) {
		std::cout << "Error: --initPhageType and --initPhageTypes both invoked. Only 1 allowed." << std::endl;
		return(0);
	}

	/// random number generator init
	srand((int)getpid());
	g::generator = std::default_random_engine(rand());
	/// curses mode init
	if (g::visualize)
		initCursesMode();
	/// set up random number generators
	Host::dNormalDoubling.param( std::normal_distribution<float>::param_type(0, 15) );
	Phage::dNormalTransductionMu.param( std::normal_distribution<float>::param_type(0, 0.05) );
	Phage::dBinomialInfect.param( std::binomial_distribution<>::param_type(5, g::phageInfectionRate) );
	g::dUniform.param( std::uniform_real_distribution<float>::param_type(0.0f, 1.0f) );
	/// world and population setup
	World world(g::xdim, g::ydim);
	world.seed(g::initHostPhage[0], g::initHostPhage[1]);
	while (g::update < g::updates) {
		world.step();
		if ((g::update&15) == 15) world.collectData();
		if (g::visualize) {
			for (int x=0; x<g::xdim; x++) {
				for (int y=0; y<g::ydim; y++) {
					if ((world.phages.grid[x][y].size() > 0) && (world.hosts.grid[x][y].size() == 0)) {
						mvaddch(y,x*2,'*'|COLOR_PAIR((short)COLOR::Phage));
						addch('*'|COLOR_PAIR((short)COLOR::Phage));
					} else if (world.hosts.grid[x][y].size() > 0) {
						if (world.hosts.grid[x][y].front()->hasResistanceAllele==true) {
							if (world.hosts.grid[x][y].front()->infected==true) {
								mvaddch(y,x*2,'*'|COLOR_PAIR((short)COLOR::riHost));
								addch('*'|COLOR_PAIR((short)COLOR::riHost));
							} else {
								mvaddch(y,x*2,'*'|COLOR_PAIR((short)COLOR::ruHost));
								addch('*'|COLOR_PAIR((short)COLOR::ruHost));
							}
						} else {
							if (world.hosts.grid[x][y].front()->infected==true) {
								mvaddch(y,x*2,'*'|COLOR_PAIR((short)COLOR::siHost));
								addch('*'|COLOR_PAIR((short)COLOR::siHost));
							} else {
								mvaddch(y,x*2,'*'|COLOR_PAIR((short)COLOR::suHost));
								addch('*'|COLOR_PAIR((short)COLOR::suHost));
							}
						}
					} else {
						mvaddch(y,x*2,'*'|COLOR_PAIR((short)COLOR::Black));
						addch('*'|COLOR_PAIR((short)COLOR::Black));
					}
				}
			}
			int row(0);
			mvaddstr(++row, g::xdim*2+2, ("update: " + std::to_string(g::update) + "     ").c_str());
			mvaddstr(++row, g::xdim*2+2, ("hosts: " + std::to_string(world.hosts.size()) + "     ").c_str());
			mvaddstr(++row, g::xdim*2+2, ("phages: " + std::to_string(world.phages.size()) + "     ").c_str());
			mvaddstr(++row, g::xdim*2+2, ("mean TR: " + std::to_string(VAULT::back().meanTransduction).substr(0,6) + "     ").c_str());
			++row;
			mvaddch(++row, g::xdim*2+2, '*'|COLOR_PAIR((short)COLOR::ruHost)); addch('*'|COLOR_PAIR((short)COLOR::ruHost));
			mvaddstr(row, g::xdim*2+6, "Resistant Uninfected host");
			mvaddch(++row, g::xdim*2+2, '*'|COLOR_PAIR((short)COLOR::riHost)); addch('*'|COLOR_PAIR((short)COLOR::riHost));
			mvaddstr(row, g::xdim*2+6, "Resistant Infected host");
			mvaddch(++row, g::xdim*2+2, '*'|COLOR_PAIR((short)COLOR::suHost)); addch('*'|COLOR_PAIR((short)COLOR::suHost));
			mvaddstr(row, g::xdim*2+6, "Susceptible Uninfected host");
			mvaddch(++row, g::xdim*2+2, '*'|COLOR_PAIR((short)COLOR::siHost)); addch('*'|COLOR_PAIR((short)COLOR::siHost));
			mvaddstr(row, g::xdim*2+6, "Susceptible Infected host");
			mvaddch(++row, g::xdim*2+2, '*'|COLOR_PAIR((short)COLOR::Phage)); addch('*'|COLOR_PAIR((short)COLOR::Phage));
			mvaddstr(row, g::xdim*2+6, "phage");
			++row;
			++row;
			if (g::drugPresent) {
				mvaddch(row, g::xdim*2+2, ' '|COLOR_PAIR((short)COLOR::Highlight));
				addch('O'|COLOR_PAIR((short)COLOR::Highlight));
				addch('N'|COLOR_PAIR((short)COLOR::Highlight));
			} else {
				mvaddch(row, g::xdim*2+2, 'O'|COLOR_PAIR((short)COLOR::NoHighlight));
				addch('F'|COLOR_PAIR((short)COLOR::NoHighlight));
				addch('F'|COLOR_PAIR((short)COLOR::NoHighlight));
			}
			mvaddstr(row, g::xdim*2+5, " antibiotic presence");
			if (world.hosts.size()+world.phages.size() == 0) sadface();
			refresh();
			if (std::tolower(getch()) == 'q') { /// exit trigger in visualize mode
				endwin();
				saveData();
				exit(0);
			}
		}
		if ((userExitFlag == 1) && (g::visualize == false)) { /// exit trigger in console mode
			saveData();
			exit(0);
		}
	}
	saveData();
	if (g::visualize) endwin();
	return(0);
}

/// Population
template <class T>
Population<T>::Population(unsigned int numInitElements, const T& value) :	std::list<std::shared_ptr<T>>(numInitElements, value),
																										dBinomialDeath(new std::binomial_distribution<>(1.0,0.5))
{ }

template <class T>
Population<T>::Population(unsigned int numInitElements) :	std::list<std::shared_ptr<T>>(numInitElements),
																							dBinomialDeath(new std::binomial_distribution<>(1.0,0.5))
{ }

template <class T>
Population<T>::Population() :	dBinomialDeath(new std::binomial_distribution<>(1.0,0.5)) { }

template <class T>
void Population<T>::doMoranProcessDeath(float deathRate, bool ignoreResistant) {
	if (this->size() == 0) return;
	/// set the dBinomialDeath distribution parameters if they currently differ from what is needed
	if ( (dBinomialDeath->t() != this->size()) || (dBinomialDeath->p() != deathRate) ) {
		dBinomialDeath->param(std::binomial_distribution<>::param_type(this->size(), deathRate));
	}
	/// use dBinomialDeath to get the expected number of dead
	int nDead = dBinomialDeath->operator()(g::generator);
	/// organisms to be removed
	std::vector<int> deathRow(nDead,0);
	for (auto& target : deathRow) {
		target = rand()%this->size();
	}
	std::sort(deathRow.begin(), deathRow.end());
	auto newEnd = std::unique(deathRow.begin(), deathRow.end());
	deathRow.erase(newEnd, deathRow.end());
	/// remove dead from all lists
	int counter(0);
	auto iterator = this->begin();
	auto next = this->begin();
	for (auto& target : deathRow) {
		while (counter < target) {
			++counter;
			iterator = std::next(iterator);
		}
		if (iterator != this->end())
			next = std::next(iterator);
		/// remove agent
		if ( (ignoreResistant) && ((*iterator)->hasResistanceAllele) ) { /* ignore */ }
		else {
			grid[(*iterator)->x][(*iterator)->y].remove( (*iterator) ); /// remove from grid
			this->erase(iterator); /// remove from population list
		}
		++counter;
		iterator = next;
	}
}

template <class T>
void Population<T>::printSize() {
	std::cout << "pop size is " << this->size() << std::endl;
}

/// World
World::World(int xdim, int ydim) : xdim(xdim), ydim(ydim) {
	hosts.grid.clear();
	phages.grid.clear();
	hosts.grid.resize(xdim);
	phages.grid.resize(xdim);
	for (auto &row : hosts.grid) { row.resize(ydim); }
	for (auto &row : phages.grid) { row.resize(ydim); }
}
template <class T>
std::shared_ptr<T>  World::tryAddAgent(Population<T> &population, bool sharesSpace, std::shared_ptr<Agent> pParent, int radius) {
	/// returns a random position within the world
	std::shared_ptr<T> agent(std::make_shared<T>());
	agent->x=0; agent->y=0;
	if (pParent!=nullptr) {
		while((agent->x==0) && (agent->y==0)) {
			agent->x = rand()%(radius*2+1)-radius;
			agent->y = rand()%(radius*2+1)-radius;
		}
		agent->x += pParent->x;
		agent->y += pParent->y;
	} else {
		agent->x = rand()&(xdim-1);
		agent->y = rand()&(ydim-1);
	}
	agent->x = (agent->x+xdim) & (xdim-1); /// assumes xdim is power of 2
	agent->y = (agent->y+ydim) & (ydim-1); /// assumes ydim is power of 2
	bool addNewAgent = true;
	if (sharesSpace == false) {
		if (population.grid[agent->x][agent->y].size() > 0) {
			agent = population.grid[agent->x][agent->y].front();
			agent->reinit();
			addNewAgent = false;
		}
	}
	if (addNewAgent) {
		population.grid[agent->x][agent->y].push_back(agent);
		population.push_back(agent);
	}
	return agent;
}
void World::step() {
	++g::update;
	++g::updatesSinceLastDrugEvent;
	if (g::debug) std::cout << "beginning update..." << std::endl;
	if (g::debug) std::cout << "size: " << hosts.size() << ", " << phages.size() << std::endl;
	/// drug application
	if (g::drugIntervals[0] > 0) {
		if (g::update == g::drugIntervals[0]) {
			if (g::drugIntervals[1] == 0)
				g::drugPresent = false;
			else
				g::drugPresent = true;
			g::updatesSinceLastDrugEvent=0;
	  	}
		if ((g::drugIntervals[1] > 0) && (g::update > g::drugIntervals[0])) {
			if ( (g::updatesSinceLastDrugEvent == g::drugIntervals[1]) && (g::drugPresent == true) && (g::drugIntervals[1] > 0) ) /// is on, turn off
				{ g::drugPresent = false; g::updatesSinceLastDrugEvent = 0; }
			if ( (g::updatesSinceLastDrugEvent == g::drugIntervals[2]) && (g::drugPresent == false) && (g::drugIntervals[2] > 0) ) /// is off, turn on
				{ g::drugPresent = true; g::updatesSinceLastDrugEvent = 0; }
		}
	}
	/// phage movement here?
	std::list< Population<Host>::iterator > deadList;
	if (g::debug) std::cout << "beginning loop" << std::endl;
	for (auto ppHost=hosts.begin(); ppHost!=hosts.end(); ++ppHost) {
		/// host duplication
		if ((*ppHost)->nextDoublingTime == g::update) {
			if (g::debug) std::cout << "a host is doubling" << std::endl;
			if ((*ppHost)->infected) {
				if (g::debug) std::cout << "a host is infected" << std::endl;
				deadList.push_back( ppHost );
				auto iLastPhage = std::prev(phages.end());
				//phages.resize( phages.size() + g::burstSize );
				if (g::debug) std::cout << "beginning burst loop" << std::endl;
				std::shared_ptr<Phage> newphage;
				for (int phagei=0; phagei<g::burstSize; ++phagei) {
					newphage = tryAddAgent(phages, true, (*ppHost)->infector, g::phageRadius); // guaranteed to be a new pointer
					newphage->inheritFrom( (*ppHost)->infector );
				}
				Population<Phage>::iterator ppPhageEnd = std::next(iLastPhage);
				/// set up transducing phage
				std::advance(ppPhageEnd, g::burstSize*(*ppHost)->infector->transductionRate);
				if (g::debug) std::cout << "beginning transducing phage setup loop" << std::endl;
				for (auto ppPhage=std::next(iLastPhage); ppPhage!=ppPhageEnd; ++ppPhage) {
					(*ppPhage)->hasResistanceAllele = (*ppHost)->hasResistanceAllele;
					(*ppPhage)->isTransducing = true;
				}
			} else { /// not host->infected
				/// if host can reproduce into a vacant spot, then add the new host
				std::shared_ptr<Host> newHost = tryAddAgent(hosts, false, *ppHost, g::hostRadius);
				newHost->setNextDoublingTime();
				newHost->inheritFrom( *ppHost );
				(*ppHost)->setNextDoublingTime();
			}
		} else { /// not doubling or bursting now
			if (phages.grid[(*ppHost)->x][(*ppHost)->y].size() > 0) { /// if phages also exist with this host
				if (g::debug) std::cout << "a host has phages nearby" << std::endl;
				if ((unsigned long)Phage::dBinomialInfect.t() != phages.grid[(*ppHost)->x][(*ppHost)->y].size()) 
					Phage::dBinomialInfect.param( std::binomial_distribution<>::param_type( phages.grid[(*ppHost)->x][(*ppHost)->y].size(), g::phageInfectionRate ) );
				int nInfectors = Phage::dBinomialInfect( g::generator );
				/// organisms to be removed
				std::vector<int> deathRow(nInfectors,0);
				if (g::debug) {
					if (nInfectors > 0)
						std::cout << "creating phage to infect the cell " << nInfectors << std::endl;
				}
				for (auto& target : deathRow) {
					target = rand()%phages.grid[(*ppHost)->x][(*ppHost)->y].size();
				}
				std::sort(deathRow.begin(), deathRow.end());
				auto newEnd = std::unique(deathRow.begin(), deathRow.end());
				deathRow.erase(newEnd, deathRow.end());
				/// remove dead from all lists
				int counter(0);
				auto itPhage = phages.grid[(*ppHost)->x][(*ppHost)->y].begin();
				auto next = phages.grid[(*ppHost)->x][(*ppHost)->y].begin();
				for (auto& target : deathRow) {
					while (counter < target) {
						++counter;
						itPhage = std::next(itPhage);
					}
					if (itPhage != phages.grid[(*ppHost)->x][(*ppHost)->y].end())
						next = std::next(itPhage);
					/// agent infects?
					if ((*itPhage)->born != g::update) { /// if not born now, then infect
						if (((*ppHost)->infected == false) && ((*itPhage)->isTransducing == false)) {
							(*ppHost)->infected = true;
							(*ppHost)->infector = *itPhage;
							(*ppHost)->setNextDoublingTime();
						}
						if ((*itPhage)->isTransducing)
							(*ppHost)->hasResistanceAllele = (*itPhage)->hasResistanceAllele;
						phages.remove( (*itPhage) ); /// remove from grid
						phages.grid[(*itPhage)->x][(*itPhage)->y].erase(itPhage); /// remove from population list
					}
					++counter;
					itPhage = next;
				}
			}
		}
	}
	for (auto& ppHost : deadList) {
		hosts.grid[(*ppHost)->x][(*ppHost)->y].remove( *ppHost );
		hosts.erase( ppHost );
	}
	if (g::drugPresent)
		hosts.doMoranProcessDeath( std::fminf(1.0f, g::hostDeathRate + g::drugRate), true );
	hosts.doMoranProcessDeath( g::hostDeathRate );
	phages.doMoranProcessDeath( g::phageDeathRate );
}
void World::seed(unsigned int numHosts, unsigned int numPhages) {
	if (g::debug) std::cout << "seeding..." << std::endl;
	/// add new agents to the world
	/// and clean up those we don't add (which might happen because we're lazy and only try placing once)
	for (int hosti=0; hosti<(int)numHosts; ++hosti) {
		tryAddAgent(hosts, false);
	}
	for (int phagei=0; phagei<(int)numPhages; ++phagei) {
		tryAddAgent(phages, true);
	}
	/// set the fraction of resistant hosts specified (--initHostsResistant [0-1])
	for (auto& pHost : hosts) if (g::dUniform(g::generator) < g::initHostsResistant) pHost->hasResistanceAllele = true;
	/// set the fraction of phages and their types
	if (g::initPhageType[0] >= 0.0f) {
		for (auto& pPhage : phages) if (g::dUniform(g::generator) < g::initPhageType[1]) pPhage->transductionRate = g::initPhageType[0];
	}
	if (g::initPhageTypes[0] >= 0.0f) {
		for (auto& pPhage : phages)	if (g::dUniform(g::generator) < g::initPhageTypes[2]) pPhage->transductionRate = g::initPhageTypes[0];
												else pPhage->transductionRate = g::initPhageTypes[1];
	}
	if (g::debug) std::cout << "seeded initial population with " << hosts.size() << " hosts and " << phages.size() << " phages." << std::endl;
}
void World::collectData() {
	VAULT::Record* rec = VAULT::newRecord();
	rec->update = g::update;
	rec->drugPresent = g::drugPresent;
	rec->nHosts = hosts.size();
	rec->nPhages = phages.size();
	rec->nRUHosts = 0;
	rec->nRIHosts = 0;
	rec->nSUHosts = 0;
	rec->nSIHosts = 0;
	rec->nTPhages = 0;
	rec->nNPhages = 0;
	for (auto& host : hosts) {
		if (host->hasResistanceAllele)
			if (host->infected)
				++rec->nRIHosts;
			else
				++rec->nRUHosts;
		else
			if (host->infected)
				++rec->nSIHosts;
			else
				++rec->nSUHosts;
	}
	for (auto& phage : phages) {
		if (phage->isTransducing)
			++rec->nTPhages;
		else
			++rec->nNPhages;
	}
	/// find average transduction rate
	rec->meanTransduction = std::accumulate(phages.begin(),
														phages.end(),
														0.0,
														[](float sum, const std::shared_ptr<Phage> pPhage){ return sum + pPhage->transductionRate; });
	rec->meanTransduction /= (float)phages.size();
	auto itMax = std::max_element(phages.begin(),
														phages.end(),
														[](const std::shared_ptr<Phage> pPhage1, const std::shared_ptr<Phage> pPhage2){ return std::fmaxf(pPhage1->transductionRate, pPhage2->transductionRate); });
	if (phages.size() > 0)
		rec->maxTransduction = (*itMax)->transductionRate;
	else
		rec->maxTransduction = 0.0;
}

/// Agent
Agent::Agent() : born(g::update), x(0), y(0), hasResistanceAllele(false) { }

/// Phage
Phage::Phage() : Agent(), transductionRate(0.0f), isTransducing(false) { }
void Phage::inheritFrom(std::shared_ptr<Agent> _ancestor) {
	std::shared_ptr<Phage> ancestor = std::static_pointer_cast<Phage>(_ancestor);
	if (g::dUniform(g::generator) < g::phageMu) {
		if (g::uniformMutations)
			this->transductionRate = std::fmaxf(0.0, std::fminf(1.0, this->transductionRate + g::dUniform(g::generator)));
		else
			this->transductionRate = std::fmaxf(0.0, std::fminf(1.0, this->transductionRate + Phage::dNormalTransductionMu(g::generator)));
	} else {
		this->transductionRate = ancestor->transductionRate;
	}
}
void Phage::reinit() { }

/// Host
Host::Host() : Agent(), infected(false), infector(nullptr) {
	setNextDoublingTime();
}
void Host::setNextDoublingTime() {
	/// resistant hosts take longer to double
	if (this->infected) {
			this->nextDoublingTime = g::update + g::phageLyseDelay + std::roundf( Host::dNormalDoubling( g::generator ) );
	} else {
		if (this->hasResistanceAllele)
			this->nextDoublingTime = g::update + g::hostDoublingTime + std::roundf( Host::dNormalDoubling( g::generator ) + g::resistanceCost*g::hostDoublingTime );
		else
			this->nextDoublingTime = g::update + g::hostDoublingTime + std::roundf( Host::dNormalDoubling( g::generator ) );
	}
}
void Host::inheritFrom(std::shared_ptr<Agent> _ancestor) {
	std::shared_ptr<Host> ancestor = std::static_pointer_cast<Host>(_ancestor);
	if (g::dUniform(g::generator) < g::hostMu)	this->hasResistanceAllele = rand()&1;
	else 														this->hasResistanceAllele = ancestor->hasResistanceAllele;
}
void Host::reinit() {
	this->nextDoublingTime = 0;
	this->infected = false;
	if(this->infector) this->infector.reset();
	this->hasResistanceAllele = false;
}
