CXX=g++
CPPFLAGS=-std=c++11 -lcurses

release: CXX += -O3
release: transduction

debug: CXX += -gdwarf-3 -g3 -pg
debug: transduction

transduction: main.o
	$(CXX) $(CPPFLAGS) -o transduction $^

clean:
	rm *.o transduction
